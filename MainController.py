from ProductDto import *
from PyQt5.QtCore import QObject, pyqtSlot


class MainController:

    def addProduct(self, productName, photo, price, rating, amount):
        return ProductDto(productName=productName, photo=photo, price=price, rating=rating, amount=amount).save()

    def updateProduct(self, productName, amount):
        return ProductDto.update({ProductDto.amount: amount}).where(ProductDto.productName == productName)
    @pyqtSlot()
    def getAllProducts(self):
        return ProductDto.select()

    def getProductsInPriceBetween(self, lowerPrice, upperPrice):
        return ProductDto.select().where(ProductDto.price.between(lowerPrice, upperPrice))

    def getProductsByNamePart(self, namePart):
        return ProductDto.select().where(ProductDto.productName.contains(namePart))

    def getProductsSortedByRating(self):
        return ProductDto.select().order_by(ProductDto.rating)

    def getProductsSortedByName(self):
        return ProductDto.select().order_by(ProductDto.productName)

    def getProductsSortedByPrice(self):
        return ProductDto.select().order_by(ProductDto.price)
