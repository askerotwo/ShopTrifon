from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal
from product import Product
from ProductDto import *
from OrderedProductsDto import *
from OrdersDto import *
from random import randint

class ShopPage(QObject):
    def __init__(self, engine):
        super(ShopPage, self).__init__()
        self.engine = engine
        self.productslist = []

    def setBasket(self, basket):
        self.basket = basket
    # signal-slot for changing basket spinbox value after clicking shop page spinbox
    basketqSignal = pyqtSignal(int, arguments=['prodid'])
    @pyqtSlot(int)
    def basketq_upd(self,id):
        self.basketqSignal.emit(id)
    
    @pyqtSlot(str, int, int, str, str, float, int)
    def addProduct(self, product_id, name, image_first, image_second, rating, quantity, price, quantityinbasket):
        x = Product(product_id, name, image_first, image_second, rating, quantity, price, quantityinbasket)
        self.productslist.append(x)

    # @pyqtSlot()
    # def hello(self):
    #     print('Hello')

    @pyqtSlot(int, int)
    def updateProductQuantityInBasket(self, id, quantity):
        for i in self.productslist:
            if(int(i.product_id) == int(id)):
                i.quantityinbasket = quantity

    def fillthelist(i):
        x = Product(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, 0)
        self.productslist.append(x)

    @pyqtSlot(int, result = str)
    def getProductName(self, index):
        return self.productslist[index].name

    @pyqtSlot(int, result = int)
    def getProductPrice(self, index):
        return self.productslist[index].price

    @pyqtSlot(int, result = int)
    def getProductPriceById(self, id):
        for i in self.productslist:
            if(int(i.product_id) == int(id)):
                return i.price

    @pyqtSlot(int, result = int)
    def getProductQuantity(self, index):
        return self.productslist[index].quantity
    

    @pyqtSlot(int, result = str)
    def getProductImage_first(self, index):
        return self.productslist[index].image_first

    @pyqtSlot(int, result = str)
    def getProductImage_second(self, index):
        return self.productslist[index].image_second

    @pyqtSlot(int, result = float)
    def getProductRating(self, index):
        return self.productslist[index].rating

    @pyqtSlot(int, result = int)
    def getProductId(self, index):
        return int(self.productslist[index].product_id)

    @pyqtSlot(int, result = int)
    def getProductQuantityInBasket(self, index):
        return int(self.productslist[index].quantityinbasket)

    @pyqtSlot(int, result = int)
    def getProductQuantityInBasketById(self, id):
        for i in self.productslist:
            if(int(i.product_id) == int(id)):
                return i.quantityinbasket

    @pyqtSlot(result = int)
    def getProductsListSize(self):
        return len(self.productslist)

    @pyqtSlot()
    def clearProductsList(self):
        self.productslist.clear()

    @pyqtSlot()
    def setMainController(self, maincontroller):
        self.maincontroller = maincontroller

    @pyqtSlot()
    def returnDefaultPage(self):
        x = self.maincontroller.getAllProducts()

    def addProductInDb(self, productName, image_first, image_second, rating, amount, price):
        ProductDto(productName=productName, image_first=image_first, image_second=image_second, rating=rating, amount=amount, price=price).save()

    @pyqtSlot()
    def updateProduct(self, productName, amount):
        return ProductDto.update({ProductDto.amount: amount}).where(ProductDto.productName == productName).execute()

    @pyqtSlot()
    def updateProductById(self, product_id, amount):
        return ProductDto.update({ProductDto.amount: amount}).where(ProductDto.id == product_id).execute()


    def saveOrderedProduct(self,productName, amount, price, orderId):
        OrderedProductsDto(productName=productName, amount=amount, price=price, orderId=orderId).save()
        # print(orderId)

    def saveOrder(self,price, deliveryType, address, orderId):
        OrdersDto(price=price, deliveryType=deliveryType, address=address, orderId=orderId).save()
        # print(orderId)

    def findOrderbyId(self, orderId):
        orders = OrderedProductsDto.select().where(OrderedProductsDto.orderId == orderId)
        return len(orders)

    orderSignal = pyqtSignal()
    @pyqtSlot(str, str)
    def orderAccepted(self, address, deliveryType):
        orderId = randint(0, 1000000000)
        totalPrice = 0
        while (self.findOrderbyId(orderId) != 0):
            orderId = randint(0, 1000000000)
        for basketProduct in self.basket.productslist:
            quantity_upd = basketProduct.quantity - basketProduct.quantityinbasket
            self.updateProductById(basketProduct.product_id, quantity_upd)
            currentPrice = basketProduct.getProductQuantityInBasket() * basketProduct.getProductPrice()
            totalPrice += currentPrice
            self.saveOrderedProduct(
                        basketProduct.getProductName(),
                        basketProduct.getProductQuantityInBasket(),
                        currentPrice,
                        orderId)
        address = "" if deliveryType == "самовывоз" else address
        self.saveOrder(
                totalPrice,
                deliveryType,
                address,
                orderId
                       )
        self.basket.productslist.clear()
        self.cleanBasketView()
        self.orderSignal.emit()
        

    @pyqtSlot()
    def getAllProducts(self):
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select()
        for i in x:
            quantityinbasket = 0
            for k in self.basket.productslist:
                if(i.id == k.product_id):
                    quantityinbasket = k.quantityinbasket
            self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

    @pyqtSlot(int, int)
    def getProductsInPriceBetween(self, lowerPrice = 0, upperPrice = 10000000):
        productlist_prev = list(self.productslist)
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select().where(ProductDto.price.between(lowerPrice, upperPrice))
        for i in x:
            for j in productlist_prev:
                if i.id == j.product_id:
                    quantityinbasket = 0
                    for k in self.basket.productslist:
                        if(i.id == k.product_id):
                            quantityinbasket = k.quantityinbasket
                    self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

    @pyqtSlot(str)
    def getProductsByNamePart(self, namePart):
        productlist_prev = list(self.productslist)
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select().where(ProductDto.productName.contains(namePart))
        for i in x:
            for j in productlist_prev:
                if i.id == j.product_id:
                    quantityinbasket = 0
                    for k in self.basket.productslist:
                        if(i.id == k.product_id):
                            quantityinbasket = k.quantityinbasket
                    self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

  # signal-slot for changing basket spinbox value after clicking shop page spinbox
    cleanSignal = pyqtSignal()
    @pyqtSlot()
    def cleanView(self):
        self.cleanSignal.emit()

    cleanBasketSignal = pyqtSignal()
    @pyqtSlot()
    def cleanBasketView(self):
        self.cleanBasketSignal.emit()
    
    @pyqtSlot()
    def getProductsAssorted(self):
        productlist_prev = list(self.productslist)
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select()
        for i in x:
            for j in productlist_prev:
                if i.id == j.product_id:
                    quantityinbasket = 0
                    for k in self.basket.productslist:
                        if(i.id == k.product_id):
                            quantityinbasket = k.quantityinbasket
                    self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

    @pyqtSlot()
    def getProductsSortedByRating(self):
        productlist_prev = list(self.productslist)
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select().order_by(ProductDto.rating.desc())
        for i in x:
            for j in productlist_prev:
                if i.id == j.product_id:
                    quantityinbasket = 0
                    for k in self.basket.productslist:
                        if(i.id == k.product_id):
                            quantityinbasket = k.quantityinbasket
                    self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

    @pyqtSlot()
    def getProductsSortedByName(self):
        productlist_prev = list(self.productslist)
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select().order_by(ProductDto.productName)
        for i in x:
            for j in productlist_prev:
                if i.id == j.product_id:
                    quantityinbasket = 0
                    for k in self.basket.productslist:
                        if(i.id == k.product_id):
                            quantityinbasket = k.quantityinbasket
                    self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

    @pyqtSlot(str)
    def getProductsSortedByPrice(self, direction):
        productlist_prev = list(self.productslist)
        self.clearProductsList()
        self.cleanView()
        x = ProductDto.select().order_by(ProductDto.price) if direction == 'up' else ProductDto.select().order_by(ProductDto.price.desc())
        for i in x:
            for j in productlist_prev:
                if i.id == j.product_id:
                    quantityinbasket = 0
                    for k in self.basket.productslist:
                        if(i.id == k.product_id):
                            quantityinbasket = k.quantityinbasket
                    self.addProduct(i.id, i.productName, i.image_first, i.image_second, i.rating, i.amount, i.price, quantityinbasket)

    def getAmountOfProductsInDb(self):
        print(' amount of products = ', len(self.productslist))

    def clearTable(self):
        ProductDto.delete().execute()

    @pyqtSlot(str, result = int)
    def strToInt(self, string):
        return int(string)




