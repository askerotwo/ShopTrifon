import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle {
    id: root
    width: main.width

    height: main.height
    property int numberofproducts : 0
    property real basketsum: shoppingcardpy.getTotalPice()
    color : "white"
    signal accepted (string address, string deliveryType)
    ScrollView {
        anchors.fill: parent
        ScrollBar.vertical.policy: ScrollBar.AlwaysOn
        contentHeight: columnB.height+100
        contentWidth: columnB.width
        Column {
            id: columnB
            width: root.width
            Repeater {
                id : repeater
                model: numberofproducts
                delegate: Item{
                    width : 900
                    height : 150
                    anchors.horizontalCenter: columnB.horizontalCenter
                    ProductCardInBasket{
                    id_product : shoppingcardpy.getProductId(index)
                    name : shoppingcardpy.getProductName(index)
                    rating : shoppingcardpy.getProductRating(index)
                    image_first_card : shoppingcardpy.getProductImage_first(index)
                    image_second_card : shoppingcardpy.getProductImage_second(index)
                    price: shoppingcardpy.getProductPrice(index)
                    quantity: shoppingcardpy.getProductQuantity(index)
                    }
                }
            }
            Text{
                anchors.horizontalCenter: columnB.horizontalCenter
                height: 60
                verticalAlignment: Text.AlignVCenter
                text: basketsum > 0 ? 'Сумма заказа: '+basketsum : 'Нет товаров в корзине'
            }
            RowLayout {
                width : 900
                height : 70
                anchors.horizontalCenter: columnB.horizontalCenter
                spacing: 5
                visible: basketsum > 0 ? true : false
                Rectangle {
                    Layout.preferredWidth: parent.width / 3
                    Layout.fillHeight: true
                    Text {
                        text: 'Способ доставки'
                        color: "black"
                        width : 300
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    ComboBox {
                        id: deliverytype
                        width: 150
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        textRole: "key"
                        model: ListModel {
                            ListElement { key: "самовывоз"}
                            ListElement { key: "курьером"}
                        }
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    TextField {
                        id: address
                        width: 300
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        placeholderText: deliverytype.currentIndex == 0 ? qsTr("Самовывоз со склада") : qsTr("Введите адрес")
                        readOnly:  deliverytype.currentIndex == 0 ? true : false
                    }
                }
            }
    
            RoundButton{
                anchors.horizontalCenter: columnB.horizontalCenter
                width: root.width*0.3
                text: 'Заказать'
                visible: basketsum > 0 ? true : false
                onClicked: {
                    root.accepted(address.text, deliverytype.currentText)
                }
            }
        }
    }
    Connections {
        target: shoppagepy
        function onCleanBasketSignal() {
            root.numberofproducts = 0    
            basketsum = Qt.binding(function() { return shoppingcardpy.getTotalPice()})
    
        }
    }

}
