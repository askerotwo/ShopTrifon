import sys
from shoppage import ShopPage
from shoppingcard import ShoppingCard
from ProductDto import ProductDto
from MainController import MainController
from PyQt5.QtCore import QUrl, QObject, pyqtSlot
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton
from PyQt5.QtQml import QQmlApplicationEngine
from PySide2.QtCore import QObject, Signal, Slot
from peewee import *
from OrderedProductsDto import OrderedProductsDto
from OrdersDto import OrdersDto


if __name__ == '__main__':
    app = QApplication([])
    engine = QQmlApplicationEngine()
    shoppage = ShopPage(engine)
    shoppingcard = ShoppingCard()
    shoppage.setBasket(shoppingcard)
    db = SqliteDatabase('products.db')
    db.create_tables([ProductDto])
    db.create_tables([OrderedProductsDto])
    db.create_tables([OrdersDto])
    shoppage.clearTable()
    maincontroller = MainController()
    shoppage.addProductInDb('смартфон Iphone 11', 'product_img/iphone11_1.JPG', 'product_img/iphone11_2.JPG', 4.5, 39, 53990)
    shoppage.addProductInDb('ноутбук ACER Aspire 7 A715-75G-73WN', 'product_img/acer_noyt_1', 'product_img/acer_noyt_2', 4.7, 23, 65990)
    shoppage.addProductInDb('Телевизор XIAOMI Mi TV 4A 32', 'product_img/xiomi_tv_1.jpg', 'product_img/xiomi_tv_2.jpg', 4.2, 155, 15990)
    shoppage.addProductInDb('Ноутбук ASUS ROG Zephyrus G15', 'product_img/rog_zephyrus_1.jpg','product_img/rog_zephyrus_2.jpg', 4.0, 11, 208990)
    shoppage.addProductInDb('Телевизор LG 49UK6200PLA, 49", Ultra HD 4K', 'product_img/lg_tv_1.jpg', 'product_img/lg_tv_2.jpg', 4.7, 26, 29990)
    shoppage.addProductInDb('Наушники HyperX Cloud Flight S', 'product_img/cloud_flight_1.jpg', 'product_img/cloud_flight_2.jpg', 4.6, 63, 14990)
    shoppage.addProductInDb('Видеокарта MSI GeForce RTX 3090 Gaming X Trio 24G', 'product_img/3090_1.jpg', 'product_img/3090_2.jpg', 5.0, 1, 261990)
    shoppage.addProductInDb('Мультиварка Midea MPC-6002', 'product_img/multicooker_media_mpc_1.png', 'product_img/multicooker_media_mpc_2.jpg', 4.9, 155, 6490)
    shoppage.addProductInDb('Кофемашина Philips EP1000/00', 'product_img/coffemachine_philips_1.jpg','product_img/coffemachine_philips_2.jpg', 4.6, 16, 30890)
    shoppage.addProductInDb('наушники Logitech G733 Lightspeed Black', 'product_img/logitech_g733_1.jpg', 'product_img/logitech_g733_2.jpg', 4.4, 26, 13990)
    shoppage.addProductInDb('Робот-пылесос Xiaomi Robot Vacuum-Mop Essential SKV4136GL', 'product_img/robot_pylesos_1.jpg', 'product_img/robot_pylesos_2.jpg', 4.8, 12, 14990)
    shoppage.addProductInDb('Мультиварка Lumme LU-1450 Misty Jade', 'product_img/multicooker_lumme_1.jpg', 'product_img/multicooker_lumme_2.jpg', 4.9, 1, 3490)
    shoppage.addProductInDb('Холодильник Hotpoint-Ariston HF 4201 X R', 'product_img/fridge_hotpoint_1.jpg', 'product_img/fridge_hotpoint_2.jpg', 4.3, 77, 40990)
    shoppage.addProductInDb('Автомобильный видеорегистратор Neoline Wide S22', 'product_img/videorecorder_neoline_1.jpg', 'product_img/videorecorder_neoline_2.jpg', 3.9, 8, 4490)
    shoppage.addProductInDb('Планарные наушники Audeze Mobius', 'product_img/audeze_mobius_1.webp', 'product_img/audeze_mobius_2.webp', 4.9, 11, 39000)
    shoppage.addProductInDb('Игровой ноутбук Acer Nitro 5', 'product_img/acer_nitro_1.jpg','product_img/acer_nitro_2.jpg', 5.0, 19, 59990)
    shoppage.addProductInDb('Кронштейн Hi HTN 4624B', 'product_img/support_1.jpg', 'product_img/support_2.jpg', 4.6, 33, 3690)
    shoppage.addProductInDb('Мультиварка GALAXY GL 2651', 'product_img/multicooker_galaxy_1.jpg', 'product_img/multicooker_galaxy_2.jpg', 4.1, 29, 3090)
    shoppage.addProductInDb('Зеркальный фотоаппарат Nikon D3500', 'product_img/nikon_1.jpg', 'product_img/nikon_2.jpg', 4.2, 15, 37990)
    
    shoppage.addProductInDb('Игровой компьютер ASUS ROG Strix GT15CK-RU018T', 'product_img/pc_rog_strix_1.jpg', 'product_img/pc_rog_strix_2.jpg', 4.4, 10, 83990)
    shoppage.addProductInDb('Дрель-шуруповерт Kolner KCD 18/2L', 'product_img/drill_1.jpg','product_img/drill_2.jpg', 4.9, 21, 3490)
    shoppage.addProductInDb('Перфоратор HIPER HRH18C', 'product_img/perforator_1.jpg', 'product_img/perforator_2.jpg', 4.8, 3, 7199)
    shoppage.addProductInDb('Пила циркулярная Интерскол ДП-165/1200', 'product_img/pila_1.jpg', 'product_img/pila_2.jpg', 4.5, 55, 6290)

    shoppage.getAllProducts()
    engine.rootContext().setContextProperty('shoppagepy', shoppage)
    engine.rootContext().setContextProperty('shoppingcardpy', shoppingcard)
    engine.load("mainshop.qml");
    sys.exit(app.exec_())

