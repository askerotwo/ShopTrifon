from peewee import *

db = SqliteDatabase('products.db')

class ProductDto(Model):
    id = PrimaryKeyField(unique = True)
    productName = TextField()
    image_first = TextField()
    image_second = TextField()
    rating = DoubleField()
    amount = IntegerField()
    price = DoubleField()

    class Meta:
        database = db
        order_by = 'id'
        db_table = 'products'
