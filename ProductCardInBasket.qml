import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle {
    width : parent.width
    height : parent.height
    color : "white"
    border.color: "grey"
    property int id_product
    property string name
    property int quantity_in_card : 0
    property int price
    property string image_first_card
    property string image_second_card
    property string quantity
    property real rating


    RowLayout {
        anchors.fill: parent
        anchors.topMargin: 5
        anchors.bottomMargin: 5
        anchors.leftMargin: 5
        spacing: 0
        Rectangle {
            Layout.preferredWidth: parent.width / 4
            Layout.fillHeight: true
            ProductImage {
                id: image
                image_first: image_first_card
                image_second: image_second_card
                fillMode: Image.PreserveAspectFit
                height : 50
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: 20
                anchors.bottomMargin: 20
                anchors.left : parent.left
                anchors.leftMargin: 50
            }

        }
        Rectangle {
            Layout.fillWidth: true
            Text {
                text: name
                width : 200
                wrapMode: Text.WrapAnywhere
                color: "black"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
        Rectangle {
            Layout.fillWidth: true
            Text {
                text: 'Цена: '+(price)
                color: "black"
                width : 50 
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
        Rectangle {
            Layout.fillWidth: true
            SpinBox {
                id: spinbox
                value : shoppingcardpy.getProductQuantityInBasket(id_product)
                from: 0
                to: quantity
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                onValueModified: {
                   shoppagepy.updateProductQuantityInBasket(id_product, spinbox.value)
                   shoppingcardpy.basketq_upd(id_product)                   
                   shoppingcardpy.updateProductQuantityInBasket(id_product, spinbox.value)
                   main.updateShopPage()
                   main.updateBasket()
                }
           }
        }    
        Rectangle {
            Layout.fillWidth: true
            Text {
                id: cost
                text: 'Стоимость: '+(price*spinbox.value)
                color: "black"
                width : 180 
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }

    Connections {
    // recieve signal for changing basket spinbox value
        target: shoppagepy 
        function onBasketqSignal(prodid) {
            if(id_product == prodid){
                spinbox.value = shoppingcardpy.getProductQuantityInBasket(prodid)
            }
        }
    }
}
