import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Image {
    id: image
    fillMode: Image.PreserveAspectFit
    property string image_first
    property string image_second
    source: mousearea.containsMouse ? image_first : image_second
    MouseArea {
        id: mousearea
        anchors.fill:parent
        hoverEnabled: true
    }
}

