import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: main
    visible:true
    width: 1024
    minimumWidth: 870
    height: 768
    title: qsTr("Магазин")
    property bool shoppageisactive: true
    property color popupBackGroundColor: "#b44"
    property color popupTextCOlor: "#ffffff"

    header: ToolBar {
        ToolButton {
            id: button
            icon.source: "ShopIcon.png"
            hoverEnabled: true
            width: main.width / 2
            checkable: false
            anchors.left: parent.left
            onClicked: {
                if(!shoppageisactive) {
                    stack.pop();
                    shoppageisactive = true
                    stack.push(shoppage)
                }
            }
        }

        ToolButton {
            icon.source: "ShoppingCardIcon.png"
            width: main.width / 2
            anchors.right: parent.right
            onClicked: {
                if(shoppageisactive) {
                    stack.pop();
                    shoppageisactive = false
                    stack.push(basket);
                }
            }
        }
    }

    function updateShopPage() {
        shoppage.numberofproducts = shoppagepy.getProductsListSize()
    }

    function updateBasket() {
        basket.numberofproducts = shoppingcardpy.getShoppingCardSize()
        basket.basketsum = shoppingcardpy.getTotalPice()
    }


    // Shortcut {
    //     sequence: 'F5'
    //     onActivated: {
    //         shoppage.hello()
    //     }
    // }

    // Shortcut {
    //     sequence: 'F6'
    //     onActivated: {
    //         console.log('q1 - ', shoppingcardpy.getProductQuantityInBasket(1))
    //     }
    // }


    Shortcut {
        sequence: 'F7'
        onActivated: {
            updateBasket()
        }
    }

    StackView {
        id: stack
        anchors.fill : parent
        initialItem: shoppage


        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }
        popEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        popExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }
    }

    Rectangle {
        id : rect
        visible : false
        color: 'white'
    }

    ShopPage {
        id: shoppage
        visible : false
    }

    ShopPage {
        id: shoppage2
        visible : false
    }

    Basket {
        id: basket
        visible: false
        basketsum: 0
    }
    Popup {
        id: popup
        property alias popMessage: message.text

        background: Rectangle {
            color: popupBackGroundColor
        }
        modal: false
        focus: true
        closePolicy: Popup.CloseOnPressOutside
        Text {
            id: message
            text: "Ваш заказ успешно оформлен!"
            anchors.centerIn: parent
            font.pointSize: 12
            color: popupTextCOlor
        }
        onOpened: popupClose.start()
    }
    // Popup will be closed automatically in 2 seconds after its opened
    Timer {
        id: popupClose
        interval: 2000
        onTriggered: popup.close()
    }
    Connections {
        target: basket
        function onAccepted(address, deliveryType) {
            shoppagepy.orderAccepted(address, deliveryType)
            popup.open()    
        }
    }
}
