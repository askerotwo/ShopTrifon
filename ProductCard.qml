import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle {
    width : 250
    height : 350
    color : "white"
    border.color: "gray"
    property int id_product
    property string name
    property int price
    property string image_first_card
    property string image_second_card
    property int quantity_in_card : 0
    property int quantity
    property real rating
    property int start_q_in_basket
    ProductImage {
        id: image
        image_first: image_first_card
        image_second: image_second_card
        fillMode: Image.PreserveAspectFit
        height : parent.height * 0.35
        anchors.top : parent.top
        anchors.topMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Flow{
        id: name_flow
        Text {
            text: name
            wrapMode: Text.Wrap
            width: parent.width
            color: "black"
        }
        anchors.top: image.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: parent.verticalCenter
        width: parent.width // important to set value
    }

    GridLayout {
        width: parent.width
        columns : 2
        rows : 3
        anchors.top: name_flow.bottom
		anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20

        Text {
            text: 'Цена'
            color: "black"
            Layout.row: 0
            Layout.column: 0
        }

        Text {
            text: price
            color: "black"
            Layout.row: 0
            Layout.column: 1
            Layout.alignment: Qt.AlignRight
        }

        Text {
            text: 'Наличие'
            color: "black"
            Layout.row: 1
            Layout.column: 0
        }

        Text {
            text: quantity
            color: "black"
            Layout.row: 1
            Layout.column: 1
            Layout.alignment: Qt.AlignRight
        }
        Text {
            text: 'Рейтинг'
            color: "black"
            Layout.row: 2
            Layout.column: 0
        }

        Text {
            text: rating
            color: "black"
            Layout.row: 2
            Layout.column: 1
            Layout.alignment: Qt.AlignRight
        }

    }

    RoundButton {
        id : buybutton
        width : parent.width*0.7
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        text: quantity==0 ? 'Нет в наличии':'Добавить в корзину'
        visible: start_q_in_basket>0 ? false:true
        onClicked: {
            if (quantity!=0){
                visible = false
                quantity_in_card = 1
                spinbox.value = 1
                spinbox.visible = true
                shoppingcardpy.addProduct(id_product, name, image_first_card, image_second_card, rating, quantity, price, 1)
                shoppagepy.updateProductQuantityInBasket(id_product, 1)
                // update spinbox on basket page
                main.updateBasket()
                shoppagepy.basketq_upd(id_product)
                main.updateShopPage()
            }
        }
    }

    SpinBox {
        id: spinbox
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        visible: start_q_in_basket>0 ? true:false
        value: start_q_in_basket
        from: 0
        to: quantity
        onValueModified: {
            quantity_in_card = spinbox.value
            shoppagepy.updateProductQuantityInBasket(id_product, spinbox.value)
            shoppingcardpy.updateProductQuantityInBasket(id_product, spinbox.value)
            if(value != 0){ 
                // update spinbox on basket page
                shoppagepy.basketq_upd(id_product)
            }
            else{
                buybutton.visible = true
                visible = false
            }
            main.updateShopPage()
            main.updateBasket()
            
        }
    }
    Connections {
        // recieve signal for changing shop page spinbox value
        target: shoppingcardpy
        function onBasketqSignal(prodid) {
            if(id_product == prodid){
                spinbox.value = shoppagepy.getProductQuantityInBasketById(prodid)
                if(spinbox.value == 0) {
                    spinbox.visible = false
                    buybutton.visible = true
                }
            }
            
        }
    }

}
