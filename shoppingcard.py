from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal
from product import Product
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton
from PyQt5.QtQuick import QQuickView, QQuickWindow

class ShoppingCard(QObject):
    def __init__(self):
        super(ShoppingCard, self).__init__()
        self.productslist = []
        
    # signal-slot for changing shop page spinbox value after clicking basket spinbox
    basketqSignal = pyqtSignal(int, arguments=['prodid'])
    @pyqtSlot(int)
    def basketq_upd(self,id):
        self.basketqSignal.emit(id)
    
    @pyqtSlot(int, str, str, str, float, int, int,int)
    def addProduct(self, product_id, name, image_first, image_second, rating, quantity, price, quantityinbasket):
        x = Product(product_id, name, image_first, image_second, rating, quantity, price, quantityinbasket)
        self.productslist.append(x)

    @pyqtSlot(int)
    def removeProductById(self, id):
        for i in range(len(self.productslist)):
            if (self.productslist[i].product_id == id):
                self.productslist.pop(i)
                return

    @pyqtSlot()
    def clearShoppingCard(self):
        self.productslist.clear()

    @pyqtSlot(result = str)
    def getTotalPice(self):
        totalprice = 0
        for i in self.productslist:
            totalprice += i.getProductPrice() * i.getProductQuantityInBasket()
        return str(totalprice)

    @pyqtSlot(result = int)
    def getShoppingCardSize(self):
        return len(self.productslist)

    def getProductById(self, id):
        for i in self.productslist:
            if(i.getProductId == id):
                return this

    @pyqtSlot(int, int)
    def updateProductQuantityInBasket(self, id, quantity):
        for i in self.productslist:
            if(int(i.getProductId()) == int(id)):
                i.quantityinbasket = quantity
                if quantity==0:
                    self.removeProductById(id)

    @pyqtSlot(int, result = int)
    def getProductQuantityInBasket(self, id):
        for i in self.productslist:
            if(int(i.getProductId()) == int(id)):
                return i.quantityinbasket


    @pyqtSlot(int, result = str)
    def getProductName(self, index):
        return self.productslist[index].name

    @pyqtSlot(int, result = int)
    def getProductPrice(self, index):
        return self.productslist[index].price

    @pyqtSlot(int, result = int)
    def getProductQuantity(self, index):
        return self.productslist[index].quantity

    @pyqtSlot(int, result = str)
    def getProductImage_first(self, index):
        return self.productslist[index].image_first

    @pyqtSlot(int, result = str)
    def getProductImage_second(self, index):
        return self.productslist[index].image_second

    @pyqtSlot(int, result = float)
    def getProductRating(self, index):
        return self.productslist[index].rating

    @pyqtSlot(int, result = int)
    def getProductId(self, index):
        return int(self.productslist[index].product_id)

